package com.example.nimbbl.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.nimbbl.R;
import com.example.nimbbl.data.model.model.Catalog_Model;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Map;

import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import tech.nimbbl.checkout.sdk.NimbblCheckoutOptions;
import tech.nimbbl.checkout.sdk.NimbblCheckoutPaymentListener;
import tech.nimbbl.checkout.sdk.NimbblCheckoutSDK;

public class CatalogPageJava extends AppCompatActivity implements NimbblCheckoutPaymentListener {
    RecyclerView recyclerview_catalog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalog_page);
        recyclerview_catalog = findViewById(R.id.recyclerview_catalog);
        List list = CollectionsKt.listOf(new Catalog_Model[]{new Catalog_Model("Colourful Mandalas.", "₹ 2", "Convert your dreary device into a bright happy place with this wallpaper by Speedy McVroom", 1), new Catalog_Model("Designer Triangles.", "₹ 4", "Bold blue and deep black triangle designer wallpaper to give your device a hypnotic effect by  chenspec from Pixabay", 2)});
        this.setUpRecycelrvView(list);
    }

    public final void setUpRecycelrvView(@NotNull List items) {
        recyclerview_catalog.setLayoutManager((RecyclerView.LayoutManager)(new LinearLayoutManager(recyclerview_catalog.getContext())));
        //if your using java then change adapter context to CatalogPageJava and un-commit below line
        //recyclerview_catalog.setAdapter((RecyclerView.Adapter)(new Catalog_Adapter(items,CatalogPageJava.this)));
    }
    @Override
    public void onPaymentSuccess(Map<String, Object> map) {
        Log.d("Nimbbl demo", Integer.toString(map.size()));
        //val payload: MutableMap<String, Any>? =
        //    p0.get("payload") as MutableMap<String, Any>?
        Toast.makeText(
                this,
                "OrderId=" + map.get("order_id") + ", Status=" + map.get("status"),
                Toast.LENGTH_LONG
        ).show();
        Intent intent = new Intent(this,OrderSucessPageAcitivty.class);
        intent.putExtra("orderid", map.get("order_id").toString());
        intent.putExtra("status",map.get("status").toString());
        startActivity(intent);
    }

    @Override
    public void onPaymentFailed(String orderId) {
        //to do

    }
    public final void makePayment(@NotNull String orderId) {
        Intrinsics.checkParameterIsNotNull(orderId, "orderId");
        NimbblCheckoutOptions.Builder b = new NimbblCheckoutOptions.Builder();
        NimbblCheckoutOptions options = b.setKey("access_key_1MwvMkKkweorz0ry").setOrderId(orderId).build();
        NimbblCheckoutSDK.getInstance().init((Activity)this);
        NimbblCheckoutSDK.getInstance().checkout(options);
    }
}